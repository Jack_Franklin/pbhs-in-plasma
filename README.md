# PBHs in plasma

Requirements:

* Meson build system

To build the project, run

```bash

meson setup build
cd build
meson compile

```

The test suite can be run from inside the build directory by calling 

```bash

meson test

```

