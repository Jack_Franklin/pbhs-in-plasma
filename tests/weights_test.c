#include <stdio.h>

#include "weights.h"

int main() {

	Tent tent = {0};
	tent.xLeft = 1.0;
	tent.xCentre = 2.0;
	tent.xRight = 3.0;

	printf("Starting Weight Function Tests \n");

	if (evaluate_weight_func(tent, 1.0) != 0.0) {
		printf("Test 1 Failed, expected 0.0 but got %f instead! \n", evaluate_weight_func(tent, 1.0)); 
	}

	if (evaluate_weight_func(tent, 1.5) != 0.5) {
		printf("Test 2 Failed, expected 0.5 but got %f instead! \n", evaluate_weight_func(tent, 1.5)); 
	}
}
