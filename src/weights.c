#include "weights.h"

double evaluate_weight_func(Tent tent, double x) {

	if (tent.xLeft <= x && x <= tent.xCentre) {
		return (x - tent.xLeft)/(tent.xCentre - tent.xLeft);
	}
	else if (tent.xCentre <= x && x <= tent.xRight) {
		return (tent.xRight - x)/(tent.xRight - tent.xCentre);
	}
	else {
		return 0.0;
	}
} 
